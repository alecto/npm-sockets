var React = require('react');
var ReactDOM = require('react-dom');

var APP = require('./components/index.js');

ReactDOM.render(
  <APP />,
  document.getElementById('react-container')
);
