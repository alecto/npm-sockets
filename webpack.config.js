module.exports = {
  entry: "./client.js",
  output: {
    filename: "public/bundle.js"
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  module: {
    loaders: [
        {
            test: /\.js/,
            exclude: /node_modules/,
            loader: "babel",
            query: {
                presets: ['react'],
            }
        }
    ]
  }
}
